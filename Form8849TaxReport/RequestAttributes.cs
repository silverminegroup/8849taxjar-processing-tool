﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Form8849TaxReport
{
    public class RequestAttributes
    {
        [JsonProperty("street")]
        public string AddressLine { get; set; }

        [JsonProperty("city")]
        public string City { get; set; }

        [JsonProperty("state")]
        public string State { get; set; }

        [JsonProperty("zipcode")]
        public string ZipCode { get; set; }

        [JsonProperty("country")]
        public string Country { get { return "US"; } }

        [JsonProperty("transaction-key")]
        public string TransactionKey { get; set; }

        [JsonProperty("refund-reference-key")]
        public string RefundReferenceKey { get; set; }

        [JsonProperty("taxable-amount")]
        public decimal TaxableAmount { get; set; }

        [JsonProperty("sales-tax")]
        public decimal SalesTax { get; set; }
    }
}
