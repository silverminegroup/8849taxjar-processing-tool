﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Form8849TaxReport
{
    public static class Exceptions
    {
        public static string ReadWebExceptions(WebException wex)
        {
            if (wex.Status == WebExceptionStatus.ProtocolError)
            {
                using (HttpWebResponse r = (HttpWebResponse)wex.Response)
                {
                    using (Stream responseStream = r.GetResponseStream())
                    {
                        using (StreamReader readStream = new StreamReader(responseStream, Encoding.UTF8))
                        {
                            string strException = readStream.ReadToEnd().Replace("status", "status-code");

                            return string.IsNullOrEmpty(strException) ? ReadErrorMessage(r.StatusCode, r.StatusDescription, wex.Message) : strException;
                        }
                    }
                }
            }
            else
            {
                return ReadErrorMessage(HttpStatusCode.InternalServerError, "Internal server error", wex.Message);
            }
        }

        public static string ReadErrorMessage(HttpStatusCode status, string error, string description)
        {
            ResponseAttributes ErrorResponse = new ResponseAttributes
            {
                StatusCode = status,
                Error = error,
                Description = description
            };

            string strException = JsonConvert.SerializeObject((ResponseAttributes)ErrorResponse, JsonSettings.JsonSerializerSettings());

            return strException;
        }
    }
}
