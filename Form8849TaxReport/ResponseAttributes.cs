﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Form8849TaxReport
{
    public class ResponseAttributes
    {
        [JsonProperty("status-code")]
        public HttpStatusCode StatusCode { get; set; }

        [JsonProperty("status")]
        public string TransactionStatus { get; set; }

        [JsonProperty("error")]
        public string Error { get; set; }

        [JsonProperty("detail")]
        public string Description { get; set; }

        [JsonProperty("transaction-key")]
        public string TransactionKey { get; set; }

        [JsonProperty("refund-reference-key")]
        public string RefundReferenceKey { get; set; }

        [JsonProperty("tax-rate")]
        public decimal TaxRate { get; set; }

        [JsonProperty("tax-amount")]
        public decimal TaxAmount { get; set; }
    }
}
