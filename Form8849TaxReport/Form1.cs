﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Form8849TaxReport
{
    class DataComparer : IEqualityComparer<SalesTaxProperties>
    {
        public bool Equals(SalesTaxProperties x, SalesTaxProperties y)
        {
            return x.SalesTaxID == y.SalesTaxID;
        }

        public int GetHashCode(SalesTaxProperties obj)
        {
            return obj.SalesTaxID.GetHashCode();
        }
    }

    public partial class Form1 : Form
    {
        public SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["dbcon"].ConnectionString);

        public Form1()
        {
            InitializeComponent();
        }

        private void btnProcess_Click(object sender, EventArgs e)
        {
            List<SalesTaxProperties> listSalesTaxData = null;

            try
            {
                string SessionID = "gkqukcoeu4nvcwank20gxmip";
                string Expiry = DateTime.Now.AddMinutes(45).ToString("yyyy-MM-dd HH.mm");
                string EmailID = "support@eform2290.com";
                string Credentials = SessionID + ":" + Expiry + ":" + EmailID;
                string Authorization = Convert.ToBase64String(Encoding.Default.GetBytes(Credentials));

                string fromDate = string.Empty;
                string toDate = string.Empty;

                if (!string.IsNullOrEmpty(txtFromDate.Text.Trim()) && !string.IsNullOrEmpty(txtToDate.Text.Trim()))
                {
                    fromDate = txtFromDate.Text.Trim();
                    toDate = txtToDate.Text.Trim();
                }

                listSalesTaxData = GetSalesTaxData(fromDate, toDate).Distinct(new DataComparer()).ToList();

                if (listSalesTaxData == null || listSalesTaxData.Count <= 0)
                {
                    WriteResponse(string.Format("Batch Process: {0}, Start: {1}, Records Count: {2}\r\n\r\n", "Manual", DateTime.Now.ToString(), 0));
                    return;
                }

                WriteResponse(string.Format("Batch Process: {0}, Start: {1}, Records Count: {2}\r\n\r\n", "Manual", DateTime.Now.ToString(), listSalesTaxData.Count));

                foreach (var item in listSalesTaxData)
                {
                    string transactionStatus = item.Status == null ? null : item.Status.Trim().ToUpper();
                    string refundStatus = item.RefundStatus == null ? null : item.RefundStatus.Trim().ToUpper();
                    ResponseAttributes ResponseObject = null;

                    item.State = USStates.TaxableStates.ContainsKey(item.State) ? USStates.TaxableStates[item.State].ToString() : item.State;
                    item.State = USStates.IgnoreStates.ContainsKey(item.State) ? USStates.IgnoreStates[item.State].ToString() : item.State;

                    try
                    {
                        string taxDetails = JsonConvert.SerializeObject(item, JsonSettings.JsonSerializerSettings());
                        WriteResponse(string.Format("TaxDetails-BeforeUpdate: {0}\r\n", taxDetails));

                        if (item.Country != "US")
                        {
                            item.Description = "NON US STATE";
                        }
                        else if (item.TaxRate <= 0 || item.SalesTax <= 0)
                        {
                            item.Description = "0 TAXRATE, 0 SALESTAX";
                        }
                        else if (!USStates.TaxableStates.ContainsKey(item.State) || USStates.IgnoreStates.ContainsKey(item.State))
                        {
                            item.Description = "NON TAXABLE STATE";
                        }
                        else if (transactionStatus == "CALCULATED" && (string.IsNullOrEmpty(refundStatus) || refundStatus == "DECLINED"))
                        {
                            string ApiUrl = "https://www.eform2290.com/Sales/api/v1/sales-taxes/orders";// ConfigurationManager.AppSettings["create"].ToString();

                            ResponseObject = Services.Create(item, Authorization, ApiUrl);

                            if (ResponseObject.StatusCode == HttpStatusCode.OK)
                            {
                                item.TransactionKey = ResponseObject.TransactionKey;
                                item.TransactionDate = DateTime.Now.ToString();
                                item.Status = ResponseObject.TransactionStatus;
                                item.Description = "REPORTED TO VENDOR";
                            }
                            else
                            {
                                item.Description = "FAILED: " + ResponseObject.Description;
                            }
                        }
                        else if (transactionStatus == "CREATED" && refundStatus == "REFUNDED")
                        {
                            string ApiUrl = "https://www.eform2290.com/Sales/api/v1/sales-taxes/refunds";// ConfigurationManager.AppSettings["refund"].ToString();

                            ResponseObject = Services.Refund(item, Authorization, ApiUrl);

                            if (ResponseObject.StatusCode == HttpStatusCode.OK)
                            {
                                item.TransactionKey = ResponseObject.RefundReferenceKey;
                                item.RefundKey = ResponseObject.TransactionKey;
                                item.TransactionDate = DateTime.Now.ToString();
                                item.Status = ResponseObject.TransactionStatus;
                                item.Description = "REFUND REPORTED TO VENDOR";
                            }
                            else
                            {
                                item.Description = "FAILED: " + ResponseObject.Description;
                            }
                        }
                        else if (transactionStatus == "CALCULATED" && refundStatus == "REFUNDED")
                        {
                            item.Status = item.RefundStatus;
                            item.Description = "REFUNDED BEFORE REPORTING";
                        }
                        else
                        {
                            // TODO: Update/Delete API call
                        }

                        UpdateSalesTaxData(item.TransactionKey, item.RefundKey, item.Status, item.Description, item.TransactionDate, item.SalesTaxID);
                        taxDetails = JsonConvert.SerializeObject(item, JsonSettings.JsonSerializerSettings());
                        string responseDetails = ResponseObject == null ? "NO API CALL" : JsonConvert.SerializeObject(ResponseObject, JsonSettings.JsonSerializerSettings());
                        WriteResponse(string.Format("TaxDetails-AfterUpdate: {0}\r\nResponseDetails: {1}\r\n\r\n", taxDetails, responseDetails));
                    }
                    catch (Exception ex)
                    {
                        string taxDetails = JsonConvert.SerializeObject(item, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore, MissingMemberHandling = MissingMemberHandling.Ignore, DefaultValueHandling = Newtonsoft.Json.DefaultValueHandling.Ignore, Converters = new List<JsonConverter> { new DecimalConverter() } });
                        string responseDetails = ResponseObject == null ? "NO API CALL" : JsonConvert.SerializeObject(ResponseObject, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore, MissingMemberHandling = MissingMemberHandling.Ignore, DefaultValueHandling = Newtonsoft.Json.DefaultValueHandling.Ignore, Converters = new List<JsonConverter> { new DecimalConverter() } });
                        WriteResponse(string.Format("TaxDetails: {0}\r\nResponseDetails: {1}\r\nError: {2}\r\n\r\n", taxDetails, responseDetails, ex.Message));
                        WriteLog(string.Format("TaxDetails: {0}, ResponseDetails: {1}, Error: {2}", taxDetails, responseDetails, ex.Message), "SalesTax-Reporting");
                    }
                }
            }
            catch (Exception ex)
            {
                WriteLog(string.Format("Error: {0}", ex.Message), "SalesTax-8849-Reporting");
            }
        }

        internal List<SalesTaxProperties> GetSalesTaxData(string fromDate, string toDate)
        {
            List<SalesTaxProperties> listSalesTaxData = new List<SalesTaxProperties>();

            try
            {
                string query = QuerySGetalesTaxData();

                DataTable dt = new DataTable();

                try
                {
                    SqlCommand cmd = new SqlCommand(query, con);
                    cmd.CommandType = CommandType.Text;

                    if (con.State == ConnectionState.Closed)
                        con.Open();

                    SqlDataAdapter lda = new SqlDataAdapter(cmd);
                    lda.Fill(dt);

                    con.Close();

                    foreach (DataRow row in dt.Rows)
                    {
                        listSalesTaxData.Add(new SalesTaxProperties
                        {
                            SalesTaxID = row["TaxKey"].ToString(),
                            FormID = row["FormKey"].ToString(),
                            PaymentKey = row["PaymentKey"].ToString(),
                            TransactionKey = row["TransactionKey"].ToString(),
                            RefundKey = row["RefundKey"].ToString(),
                            TaxableAmout = Convert.ToDecimal(row["TaxableAmount"].ToString()),
                            TaxRate = Convert.ToDecimal(row["TaxRate"].ToString()),
                            SalesTax = Convert.ToDecimal(row["SalesTax"].ToString()),
                            AddressLine = row["AddressLine"].ToString(),
                            City = row["City"].ToString(),
                            State = row["State"].ToString(),
                            ZipCode = row["Zipcode"].ToString(),
                            Country = row["Country"].ToString(),
                            Status = row["TransactionStatus"].ToString(),
                            RefundStatus = row["RefundStatus"].ToString()
                        });
                    }
                }
                catch
                {
                    con.Close();
                }
            }
            catch (Exception ex)
            {
                WriteLog(string.Format("Error: {0}", ex.Message), "SalesTax-8849-Reporting");
            }

            return listSalesTaxData;
        }

        private void UpdateSalesTaxData(string TransactionKey, string RefundKey, string Status, string Description, string TransactionDate, string SalesTaxID)
        {
            try
            {
                string query = QueryUpdateSalesTaxData(TransactionKey, RefundKey);
                //if (TransactionKey.Length > 0)
                //{
                //    query = query.Replace("@transactionKey|", TransactionKey);
                //    query = query.Replace("null", string.Empty);
                //}
                //else
                //{
                //    query = query.Replace("'@transactionKey|'", string.Empty);
                //}
                //if (RefundKey.Length > 0)
                //{
                //    query = query.Replace("@refundKey|", RefundKey);
                //    query = query.Replace("null", string.Empty);
                //}
                //else
                //{
                //    query = query.Replace("'@refundKey|'", string.Empty);
                //}

                query = query.Replace("@trStatus", Status);
                query = query.Replace("@trDescn", Description);
                query = query.Replace("@trDate", TransactionDate);
                query = query.Replace("@UpdDate", DateTime.Now.ToString());
                query = query.Replace("@taxKey", SalesTaxID);

                WriteResponse(string.Format("-----UpdateQuery: {0}\r\n", query));

                SqlCommand cmd = new SqlCommand(query, con);
                cmd.CommandType = CommandType.Text;

                if (con.State == ConnectionState.Closed)
                    con.Open();

                cmd.ExecuteNonQuery();

                con.Close();
            }
            catch (Exception ex)
            {
                WriteLog(string.Format("Error: {0}", ex.Message), "SalesTax-8849-Reporting");
            }
        }

        private string QuerySGetalesTaxData()
        {
            return "DECLARE @fromDate varchar(20) DECLARE @toDate varchar(20)"
                    + " SET @fromDate = '" + txtFromDate.Text + "' SET @toDate = '" + txtToDate.Text + "'"
                    + " IF((@fromDate IS NULL OR @toDate IS NULL))"
                    + " BEGIN"
                        + " SET @fromDate = (SELECT FORMAT((GETDATE() ), 'yyyy-MM-dd'))"
                        + " SET @toDate = (SELECT FORMAT((GETDATE() + 1), 'yyyy-MM-dd'))"
                    + " END"
                    + " IF((LEN(LTRIM(RTRIM(@fromDate))) <= 0  OR LEN(LTRIM(RTRIM(@toDate))) <= 0))"
                    + " BEGIN"
                        + " SET @fromDate = (SELECT FORMAT((GETDATE() ), 'yyyy-MM-dd'))"
                        + " SET @toDate = (SELECT FORMAT((GETDATE() + 1), 'yyyy-MM-dd'))"
                    + " END"
                    + " SELECT"
                    + " TAX.PK_ST_KEY AS TaxKey,"
                    + " TAX.FK_8849_F_key AS FormKey,"
                    + " TAX.FK_C_PSF_key AS PaymentKey,"
                    + " TAX.Transaction_Key AS TransactionKey,"
                    + " TAX.Refund_Key AS RefundKey,"
                    + " TAX.Taxable_Amt AS TaxableAmount,"
                    + " TAX.Tax_Rate AS TaxRate,"
                    + " TAX.Sales_Tax AS SalesTax,"
                    + " CASE"
                        + " WHEN BS.addr_line1 IS NULL AND BS.addr_line2 IS NULL THEN NULL"
                        + " WHEN BS.addr_line1 = BS.addr_line2 THEN BS.addr_line1"
                        + " WHEN BS.addr_line1 IS NOT NULL AND BS.addr_line2 IS NULL THEN BS.addr_line1"
                        + " WHEN BS.addr_line2 IS NOT NULL AND BS.addr_line1 IS NULL THEN BS.addr_line2"
                        + " ELSE BS.addr_line1 + ' ' + BS.addr_line2"
                    + " END AS AddressLine,"
                    + " BS.City AS City,"
                    + " BS.State AS State,"
                    + " BS.Country AS Country,"
                    + " BS.Zip AS Zipcode,"
                    + " TAX.Status AS TransactionStatus,"
                    + " REF.Cur_Status as RefundStatus,"
                    + " FEE.crn_dt"
                    + " FROM"
                    + " [C_8849_SalesTax] TAX LEFT JOIN"
                    + " [C_Payment_Service_Fee] FEE ON FEE.PK_C_PSF_key = TAX.FK_C_PSF_key LEFT JOIN"
                    + " [C_Submissions8849] SUB ON SUB.FK_8849F_key = TAX.FK_8849_F_key LEFT JOIN"
                    + " [8849_Business_Submitted] BS ON BS.FK_8849_F_key = TAX.FK_8849_F_key LEFT JOIN"
                    + " [CM_Refund2290] REF ON  REF.ref_no = SUB.ref_no"
                    + " WHERE"
                    + " ("
                       + " TAX.Status = 'Calculated' AND REF.Refund_TransNo IS NULL"
                        + " OR TAX.Status = 'Calculated' AND REF.Refund_TransNo IS NOT NULL"
                        + " OR TAX.Status = 'Created' AND REF.Refund_TransNo IS NOT NULL"	
                    + " )"
                    + " AND"
                    + " ("
                        + " FEE.crn_dt >= @fromDate AND FEE.crn_dt < @toDate"
                        + " OR REF.Refund_Date >= @fromDate AND REF.Refund_Date < @toDate"
                    + " )"
                    //+"--AND BS.State IS NULL"
                    //+"--AND TAX.Description IS NULL"
                    //+"--AND REF.Cur_Status IS NULL"
                    //+"--and TAX.PK_ST_KEY = '5dd37ebc-1783-47c7-b075-bc09fd64fa2b'"
                    //+ " AND TAX.Taxable_Amt > 0"
                    + " ORDER BY FEE.crn_dt";
        }

        private string QueryUpdateSalesTaxData(string TransactionKey, string RefundKey)
        {
            string query = "UPDATE [C_8849_SalesTax] SET";

            if (!string.IsNullOrEmpty(TransactionKey))
            {
                query += " [Transaction_Key] = '" + TransactionKey + "' ,";
            }
            else
            {
                query += " [Transaction_Key] = null,";
            }

            if (!string.IsNullOrEmpty(RefundKey))
            {
                query += " [Refund_Key] = '" + RefundKey + "' ,";
            }
            else
            {
                query += " [Refund_Key] = null,";
            }

            query += " [Status] = '@trStatus',"
                     + " [Description] = '@trDescn',"
                     + " [Transaction_Date] = '@trDate',"
                     + " [Upd_dt] = '@UpdDate'"
                     + " WHERE [PK_ST_KEY] = '@taxKey'";

            return query;
        }

        void WriteResponse(string output)
        {
            txtResponse.AppendText(output);
            txtResponse.ScrollToCaret();
        }

        private void WriteLog(string logQuery, string logRemarks)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("[usp_ins_log]", con);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add(new SqlParameter("@executive", 0));
                cmd.Parameters.Add(new SqlParameter("@query", logQuery));
                cmd.Parameters.Add(new SqlParameter("@remarks", logRemarks));

                if (con.State == ConnectionState.Closed)
                    con.Open();

                cmd.ExecuteNonQuery();

                con.Close();
            }
            catch { }
        }
    }
}
