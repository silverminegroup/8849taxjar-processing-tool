﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Authentication;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Form8849TaxReport
{
    public class Gateways
    {
        public static string WebRequest(string ApiUrl, string WebRequestMethod, string PostData, string Authorization)
        {
            string webResponse = string.Empty;

            try
            {
                WebRequest w = System.Net.WebRequest.Create(ApiUrl);
                w.Method = WebRequestMethod;
                w.ContentType = "application/json";
                w.Headers.Add("Authorization", "Basic " + Authorization);

                if (w.Method == WebRequestMethods.Http.Post || w.Method == WebRequestMethods.Http.Put)
                {
                    using (var streamWriter = new StreamWriter(w.GetRequestStream()))
                    {
                        streamWriter.Write(PostData);
                    }
                }

                using (HttpWebResponse r = (HttpWebResponse)w.GetResponse())
                {
                    using (Stream responseStream = r.GetResponseStream())
                    {
                        using (StreamReader readStream = new StreamReader(responseStream, Encoding.UTF8))
                        {
                            webResponse = readStream.ReadToEnd();
                        }
                    }
                }

                return webResponse;
            }
            catch (WebException wex)
            {
                return Exceptions.ReadWebExceptions(wex);
            }
            catch (Exception ex)
            {
                return Exceptions.ReadErrorMessage(HttpStatusCode.InternalServerError, "Internal server error", ex.Message);
            }
        }
    }
}
