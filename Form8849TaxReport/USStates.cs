﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Form8849TaxReport
{
    public static class USStates
    {
        public static Dictionary<string, string> TaxableStates = new Dictionary<string, string>();
        public static Dictionary<string, string> IgnoreStates = new Dictionary<string, string>();

        static USStates()
        {
            TaxableStates.Add("Armed Forces Americas", "AA");
            TaxableStates.Add("Armed Forces (Europe,Canada,Middle East,Africa)", "AE");
            TaxableStates.Add("Alabama", "AL");
            TaxableStates.Add("Armed Forces Pacific", "AP");
            TaxableStates.Add("Arkansas", "AR");
            TaxableStates.Add("American Samoa", "AS");
            TaxableStates.Add("Arizona", "AZ");
            TaxableStates.Add("California", "CA");
            TaxableStates.Add("Colorado", "CO");
            TaxableStates.Add("Connecticut", "CT");
            TaxableStates.Add("Federated States of Micronesia", "FM");
            TaxableStates.Add("Georgia", "GA");
            TaxableStates.Add("Guam", "GU");
            TaxableStates.Add("Iowa", "IA");
            TaxableStates.Add("Idaho", "ID");
            TaxableStates.Add("Illinois", "IL");
            TaxableStates.Add("Indiana", "IN");
            TaxableStates.Add("Kentucky", "KY");
            TaxableStates.Add("Louisiana", "LA");
            TaxableStates.Add("Massachusetts", "MA");
            TaxableStates.Add("Maryland", "MD");
            TaxableStates.Add("Maine", "ME");
            TaxableStates.Add("Marshall Islands", "MH");
            TaxableStates.Add("Michigan", "MI");
            TaxableStates.Add("Minnesota", "MN");
            TaxableStates.Add("Northern Mariana Islands", "MP");
            TaxableStates.Add("North Carolina", "NC");
            TaxableStates.Add("North Dakota", "ND");
            TaxableStates.Add("Nebraska", "NE");
            TaxableStates.Add("New Jersey", "NJ");
            TaxableStates.Add("New Mexico", "NM");
            TaxableStates.Add("Nevada", "NV");
            TaxableStates.Add("New York", "NY");
            TaxableStates.Add("Ohio", "OH");
            TaxableStates.Add("Oklahoma", "OK");
            TaxableStates.Add("Pennsylvania", "PA");
            TaxableStates.Add("Puerto Rico", "PR");
            TaxableStates.Add("Palau", "PW");
            TaxableStates.Add("Rhode Island", "RI");
            TaxableStates.Add("South Carolina", "SC");
            TaxableStates.Add("South Dakota", "SD");
            TaxableStates.Add("Texas", "TX");
            TaxableStates.Add("Utah", "UT");
            TaxableStates.Add("Virginia", "VA");
            TaxableStates.Add("U.S. Virgin Islands", "VI");
            TaxableStates.Add("Vermont", "VT");
            TaxableStates.Add("Washington", "WA");
            TaxableStates.Add("Wisconsin", "WI");
            TaxableStates.Add("West Virginia", "WV");

            TaxableStates.Add("AA", "AA");
            TaxableStates.Add("AE", "AE");
            TaxableStates.Add("AL", "AL");
            TaxableStates.Add("AP", "AP");
            TaxableStates.Add("AR", "AR");
            TaxableStates.Add("AS", "AS");
            TaxableStates.Add("AZ", "AZ");
            TaxableStates.Add("CA", "CA");
            TaxableStates.Add("CO", "CO");
            TaxableStates.Add("CT", "CT");
            TaxableStates.Add("FM", "FM");
            TaxableStates.Add("GA", "GA");
            TaxableStates.Add("GU", "GU");
            TaxableStates.Add("IA", "IA");
            TaxableStates.Add("ID", "ID");
            TaxableStates.Add("IL", "IL");
            TaxableStates.Add("IN", "IN");
            TaxableStates.Add("KY", "KY");
            TaxableStates.Add("LA", "LA");
            TaxableStates.Add("MA", "MA");
            TaxableStates.Add("MD", "MD");
            TaxableStates.Add("ME", "ME");
            TaxableStates.Add("MH", "MH");
            TaxableStates.Add("MI", "MI");
            TaxableStates.Add("MN", "MN");
            TaxableStates.Add("MP", "MP");
            TaxableStates.Add("NC", "NC");
            TaxableStates.Add("ND", "ND");
            TaxableStates.Add("NE", "NE");
            TaxableStates.Add("NJ", "NJ");
            TaxableStates.Add("NM", "NM");
            TaxableStates.Add("NV", "NV");
            TaxableStates.Add("NY", "NY");
            TaxableStates.Add("OH", "OH");
            TaxableStates.Add("OK", "OK");
            TaxableStates.Add("PA", "PA");
            TaxableStates.Add("PR", "PR");
            TaxableStates.Add("PW", "PW");
            TaxableStates.Add("RI", "RI");
            TaxableStates.Add("SC", "SC");
            TaxableStates.Add("SD", "SD");
            TaxableStates.Add("TX", "TX");
            TaxableStates.Add("UT", "UT");
            TaxableStates.Add("VA", "VA");
            TaxableStates.Add("VI", "VI");
            TaxableStates.Add("VT", "VT");
            TaxableStates.Add("WA", "WA");
            TaxableStates.Add("WI", "WI");
            TaxableStates.Add("WV", "WV");

            IgnoreStates.Add("District of Columbia", "DC");
            IgnoreStates.Add("Florida", "FL");
            IgnoreStates.Add("Hawaii", "HI");
            IgnoreStates.Add("Kansas", "KS");
            IgnoreStates.Add("Mississippi", "MS");
            IgnoreStates.Add("Missouri", "MO");
            IgnoreStates.Add("Tennessee", "TN");
            IgnoreStates.Add("Wyoming", "WY");
            IgnoreStates.Add("Alaska", "AK");
            IgnoreStates.Add("Oregon", "OR");
            IgnoreStates.Add("Delaware", "DE");
            IgnoreStates.Add("Montana", "MT");
            IgnoreStates.Add("New Hampshire", "NH");

            IgnoreStates.Add("DC", "DC");
            IgnoreStates.Add("FL", "FL");
            IgnoreStates.Add("HI", "HI");
            IgnoreStates.Add("KS", "KS");
            IgnoreStates.Add("MS", "MS");
            IgnoreStates.Add("MO", "MO");
            IgnoreStates.Add("TN", "TN");
            IgnoreStates.Add("WY", "WY");
            IgnoreStates.Add("AK", "AK");
            IgnoreStates.Add("OR", "OR");
            IgnoreStates.Add("DE", "DE");
            IgnoreStates.Add("MT", "MT");
            IgnoreStates.Add("NH", "NH");

        }
    }
}
