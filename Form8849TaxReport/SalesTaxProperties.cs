﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Form8849TaxReport
{
    public class SalesTaxProperties
    {
        public string SalesTaxID { get; set; }

        public string FormID { get; set; }

        public string PaymentKey { get; set; }

        public string TransactionKey { get; set; }

        public string RefundKey { get; set; }

        public decimal TaxableAmout { get; set; }

        public decimal TaxRate { get; set; }

        public decimal SalesTax { get; set; }

        public string Status { get; set; }

        public string AddressLine { get; set; }

        public string City { get; set; }

        public string State { get; set; }

        public string ZipCode { get; set; }

        public string Country { get; set; }

        public string Description { get; set; }

        public string TransactionDate { get; set; }

        public string RefundStatus { get; set; }
    }
}
