﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Net;
namespace Form8849TaxReport
{
    public static class Services
    {
        //private static string ApiUrl = string.Empty;
        //private static string WebRequestMethod = string.Empty;
        //private static string RequestBody = string.Empty;

        public static bool IsAddressChanged(RequestAttributes BufferedRequestObject, RequestAttributes NewRequestObject)
        {
            bool AddressChanged = false;

            try
            {
                if (BufferedRequestObject.AddressLine != NewRequestObject.AddressLine)
                    AddressChanged = true;
                else if (BufferedRequestObject.City != NewRequestObject.City)
                    AddressChanged = true;
                else if (BufferedRequestObject.State != NewRequestObject.State)
                    AddressChanged = true;
                else if (BufferedRequestObject.ZipCode != NewRequestObject.ZipCode)
                    AddressChanged = true;
                else
                    AddressChanged = false;
            }
            catch { AddressChanged = false; }

            return AddressChanged;
        }

        public static ResponseAttributes Calculate(RequestAttributes TaxRequestObj, string Authorization, string ApiUrl)
        {
            string WebRequestMethod = WebRequestMethods.Http.Post;
            string RequestBody = JsonConvert.SerializeObject(TaxRequestObj, JsonSettings.JsonSerializerSettings());

            string webResponse = Gateways.WebRequest(ApiUrl, WebRequestMethod, RequestBody, Authorization);

            return JsonConvert.DeserializeObject<ResponseAttributes>(webResponse, JsonSettings.JsonSerializerSettings());
        }

        public static ResponseAttributes Create(SalesTaxProperties SalesTaxItem, string Authorization, string ApiUrl)
        {
            RequestAttributes RequestObject = new RequestAttributes
            {
                AddressLine = SalesTaxItem.AddressLine,
                City = SalesTaxItem.City,
                State = SalesTaxItem.State,
                ZipCode = SalesTaxItem.ZipCode,
                TaxableAmount = SalesTaxItem.TaxableAmout,
                SalesTax = SalesTaxItem.SalesTax
            };

            string WebRequestMethod = WebRequestMethods.Http.Post;

            string RequestBody = JsonConvert.SerializeObject(RequestObject, JsonSettings.JsonSerializerSettings());

            string webResponse = Gateways.WebRequest(ApiUrl, WebRequestMethod, RequestBody, Authorization);

            return JsonConvert.DeserializeObject<ResponseAttributes>(webResponse, JsonSettings.JsonSerializerSettings());
        }

        public static ResponseAttributes Refund(SalesTaxProperties SalesTaxItem, string Authorization, string ApiUrl)
        {
            RequestAttributes RequestObject = new RequestAttributes
            {
                RefundReferenceKey = SalesTaxItem.TransactionKey,
                AddressLine = SalesTaxItem.AddressLine,
                City = SalesTaxItem.City,
                State = SalesTaxItem.State,
                ZipCode = SalesTaxItem.ZipCode,
                TaxableAmount = SalesTaxItem.TaxableAmout * -1,
                SalesTax = SalesTaxItem.SalesTax * -1
            };

            string WebRequestMethod = WebRequestMethods.Http.Post;

            string RequestBody = JsonConvert.SerializeObject(RequestObject, JsonSettings.JsonSerializerSettings());

            string webResponse = Gateways.WebRequest(ApiUrl, WebRequestMethod, RequestBody, Authorization);

            return JsonConvert.DeserializeObject<ResponseAttributes>(webResponse, JsonSettings.JsonSerializerSettings());
        }
    }
}
